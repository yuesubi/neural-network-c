#include <stdio.h>

#include "neuralnet/core.h"
#include "raylib.h"


#define UNIT 4
#define CFG_LEN 3
#define PIXEL_AMNT 16384


u32 GetImage(const char *fp, u8 **oImg)
{
	Image img = LoadImage(fp);

	*oImg = NEW_ARR(u8, img.width * img.height);
	CHECK_PTR(*oImg);

	for (u32 x = 0; x < img.width; x++) {
		for (u32 y = 0; y < img.height; y++) {
			Color col = GetImageColor(img, x, y);
			(*oImg)[x * img.height + y] = col.r;
		}
	}

	u32 size = img.width * img.height;
	UnloadImage(img);

	return size;
}


void GetRndTrainElem(Vec *oIn, Vec *oOut)
{
	u8 num = GetRandomValue(0, 9);

	for (u8 i = 0; i < 10; i++) {
		Vec_set(oOut, i, (num == i) ? 1.0 : 0.0);
	}

	u8 subFolder = GetRandomValue(0, 7);
	u32 chosen = GetRandomValue(0, 999);

	char fpBuff[255];
	sprintf(fpBuff, "res/hand_drawn_digits/%d/hsf_%d/hsf_%d_%.5d.png",
		num, subFolder, subFolder, chosen);
	
	u8 *imgDataBuff;
	u32 datSize = GetImage(fpBuff, &imgDataBuff);

	if (PIXEL_AMNT != datSize) {
		ERR("Problem somewere");
	}

	for (u32 i = 0; i < datSize; i++) {
		Vec_set(oIn, i, (double) imgDataBuff[i] / 255.0);
	}

	DEL(imgDataBuff);
}


void GenTrainData(u32 amnt, Mat *oIns, Mat *oOuts)
{
	Mat ins = Mat_new(PIXEL_AMNT, amnt);
	Mat outs = Mat_new(10, amnt);

	for (u32 i = 0; i < amnt; i++) {
		Vec currIn = Vec_new(PIXEL_AMNT);
		Vec currOut = Vec_new(10);

		GetRndTrainElem(&currIn, &currOut);

		Mat_setRow(&ins, i, &currIn);
		Mat_setRow(&outs, i, &currOut);

		Vec_del(&currIn);
		Vec_del(&currOut);
	}

	*oIns = ins;
	*oOuts = outs;
}


int main()
{
	u8 *buffer;
	u32 size = GetImage("res/hand_drawn_digits/0/hsf_0/hsf_0_00000.png",
		&buffer);
	
	printf("%d", size);

	/*u32 layout[CFG_LEN] = { 16348, 64, 64, 10 };
	Net nn = Net_rnd(ActivFuncSigmoid, layout, CFG_LEN);

	Mat ins = Mat_new(1, 100);
	for (u32 i = 0; i < ins.width * ins.height; i++) {
		ins.m_Vals[i] = i / (double) (ins.width * ins.height);
	}

	Mat expect = Mat_new(1, 100);
	for (u32 i = 0; i < expect.width * expect.height; i++) {
		expect.m_Vals[i] = 0.5;
	}

	Mat rOuts = Net_fwdBatch(&nn, &ins);
	printf("Loss before: %f\n", EvalLoss(&rOuts, &expect));
	Mat_del(&rOuts);

	for (u32 i = 0; i < 10000; i++)
		BackProp(&nn, &ins, &expect);

	rOuts = Net_fwdBatch(&nn, &ins);
	printf("Loss after: %f\n", EvalLoss(&rOuts, &expect));
	Mat_print(&rOuts);
	Mat_del(&rOuts);

	Net_save(&nn, "res/net.txt");*/

	/*
	Vec v = Vec_newZero(5);
	printf("Value is %f\n", Vec_get(&v, 2));

	InitWindow(640, 460, "Recognize digits");

	while (!WindowShouldClose()) {
		BeginDrawing();
			ClearBackground(BLACK);
			DrawFPS(10, 10);
		EndDrawing();
	}

	CloseWindow();*/

	return 0;
}
