# <><><>< VARIABLES TO CHANGE ><><><>
TARGET_LIB := neuralnetwork.a
TARGET_EXEC := program.exe

BUILD_DIR  := ./bin
LIB_DIRS   := ./lib
SRC_DIRS   := ./src ./inc

CC := gcc
AR := ar
CFLAGS := -Wall -O2

WINDOWS_LINK_FLAGS := -lm
LINUX_LINK_FLAGS := -lm

# You need to give .c files yourself on windows.
# In linux it automaticly detects them
SRC_FILES := ./src/main.c


# <><><>< COMMANDS AND INSTRUCTIONS ><><><>

CURRENT_PLATFORM := none
ifeq ($(OS),Windows_NT)
	CURRENT_PLATFORM := windows
else
	UNAMEOS = $(shell uname)
	ifeq ($(UNAMEOS),Linux)
		CURRENT_PLATFORM := linux
	endif
endif


LINK_FLAGS :=
SRCS :=
INC_DIRS := 

ifeq ($(CURRENT_PLATFORM),windows)
	# Use windows's link flags
	LINK_FLAGS += $(WINDOWS_LINK_FLAGS)

	# Set C files we want to compile
	SRCS := $(SRC_FILES)

	# Folders with header files
	INC_DIRS := $(SRC_DIRS)

else ifeq ($(CURRENT_PLATFORM),linux)
	# Use linux's link flags
	LINK_FLAGS += $(LINUX_LINK_FLAGS)

	# Find all the C files we want to compile
	SRCS := $(shell find $(SRC_DIRS) -name '*.c')

	# Every folder needs to be passed to GCC so that it can find header files
	INC_DIRS := $(shell find $(SRC_DIRS) -type d)

endif


# String substitution for every C file.
# As an example, hello.c turns into ./build/hello.c.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

INC_FLAGS := $(addprefix -I,$(INC_DIRS))
LIB_FLAGS := $(addprefix -L,$(LIB_DIRS))

CPPFLAGS := $(INC_FLAGS)
LDFLAGS := $(LIB_FLAGS) $(LINK_FLAGS)


default: run


run: $(BUILD_DIR)/$(TARGET_EXEC)
	@echo "<><><>< RUNNING $< ><><><>"
	@exec $<


lib: $(TARGET_LIB)

$(TARGET_LIB): $(OBJS)
	@echo "<><><>< BUILDING LIBRAIRY $@ ><><><>"
	"$(AR)" rcs $(BUILD_DIR)/$(TARGET_LIB) $(OBJECTS)


# The final build step.
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	@echo "<><><>< LINKING ><><><>"
	"$(CC)" $(OBJS) -o $@ $(LDFLAGS)
	@echo ""

# Build step for C source
$(BUILD_DIR)/%.c.o: %.c
	@echo "<><><>< COMPILING $< TO $@ ><><><>"
	@mkdir -p $(dir $@)
	"$(CC)" $(CPPFLAGS) $(CFLAGS) -c $< -o $@
	@echo ""


setup:
	@echo "<><><>< MAKE DIRECTORIES ><><><>"
	mkdir -p $(LIB_DIRS)
	mkdir -p $(SRC_DIRS)


.PHONY: clean
clean:
	@echo "<><><>< CLEANING $(BUILD_DIR) ><><><>"
	@rm -r $(BUILD_DIR)
