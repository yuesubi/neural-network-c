#!/bin/bash

# VARIABLES
OUT=bin/release/project.exe
INP=`find ./src -name *.c | tr "\n" " "`

INC=inc/
LIB=lib/


# CLEAN THE PROMPT
clear


# DELETE PREVIOUS BUILD
if [ -f "$OUT" ]; then
    echo "[RUN] Deleting the previous $OUT ..."
    rm "$OUT"
    echo ""
fi


# COMPILE
echo "[RUN] Compiling $INP ..."
gcc $INP -o $OUT -Wall -O2 -D RELEASE -I $INC -L $LIB -lm
echo ""
