#ifndef NN_BACKPROP_H
#define NN_BACKPROP_H

#include "../net.h"
#include "../math/vector.h"


/* Back propagation functions */

double EvalLoss(const Mat *ouputsFound, const Mat *outputsWanted);

// Adjust weights of the neural network
void BackProp(Net *neuralNetwork, const Mat *inputsBatch,
	const Mat *outputsExpected);


/* Back propagation
 * 
 * - evaluate the loss of the network over the training data
 * - forward input starting from a specific neuron
 * - find the desired change
 * - average the desired changes for a layer
 * - average the desired changes for all inputs
 * - forward and get information about the calls
 */


#endif  // NN_BACKPROP_H