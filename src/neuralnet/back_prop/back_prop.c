#include "back_prop.h"

#include <stdio.h>

#include "../macros.h"


void CalcCallData(const Net *nn, const Vec *ins, Vec **oCallData,
	u32 *oCallDatLen)
{
	Vec *data = NEW_ARR(Vec, nn->size + 1);
	data[0] = Vec_cpy(ins);

	for (u32 i = 0; i < nn->size; i++) {
		data[i + 1] = Layer_fwd(&nn->layers[i], &data[i]);
	}

	*oCallData = data;
	*oCallDatLen = nn->size + 1;
}


// Find the desired change for the previous layer's neuron output
Vec NeuronDesiredPrevLayChng(const Vec *weights, double activDerivOut,
	double chngStep)
{
	Vec desPrevLayChng = Vec_new(weights->size);

	for (u32 i = 0; i < weights->size; i++) {
		double desChng = Vec_get(weights, i) * activDerivOut;
		desChng *= chngStep;
		Vec_set(&desPrevLayChng, i, desChng);
	}

	return desPrevLayChng;
}


// Find the desired change for the neuron's weights
Vec NeuronDesiredWghtsChng(const Vec *prevLayVals, double activDerivOut,
	double chngStep)
{
	Vec desWghtsChng = Vec_new(prevLayVals->size);

	for (u32 i = 0; i < prevLayVals->size; i++) {
		double desChng = Vec_get(prevLayVals, i) * activDerivOut;
		desChng *= chngStep;
		Vec_set(&desWghtsChng, i, desChng);
	}

	return desWghtsChng;
}


// Find the desired change to the neuron's bias 
double NeuronDesiredBiasChng(double activDerivOut, double chngStep)
{
	return activDerivOut * chngStep;
}


/* Gets the desired change to the neurons weights and biases.
 * :param nn: The network used to produce the callData.
 * :param callData: The inputs given plus the output given by each neuron by
 * using those inputs.
 * :param callDatLen: The length of the callData array.
 * :param expect: The outputs desired to be given by the network when using that
 * input data.
 * :return: A network that corresponds to the disired changes
 */
Net GetDesChng(const Net *nn, const Vec *callData, u32 callDatLen, Vec *expect)
{
	// Security verification
	if (nn->size != callDatLen - 1) {
		printf("GetDesChng: The callData length should be +1 compared to the nn size as it also contains the inputs\n");
		exit(EXIT_FAILURE);
	}

	// Initialising a network that will contain the desired changes
	Net desChng;
	desChng.size = nn->size;
	desChng.inLen = nn->inLen;
	desChng.outLen = nn->outLen;

	desChng.layers = NEW_ARR(Layer, desChng.size);
	CHECK_PTR(desChng.layers);

	// Vec to contain what derired changes of the previous layer are
	Vec prevExpect = Vec_cpy(expect);

	/* For every layer, going backwards (i64 because if under 0 we are in
	trouble) */
	for (i64 i = nn->size - 1; i >= 0; i--) {
		// The callData of the layer
		const Vec *currLay = &callData[i + 1];

		// Setting up the layer
		desChng.layers[i].size = nn->layers[i].size;
		desChng.layers[i].neurons = NEW_ARR(Neuron, currLay->size);
		CHECK_PTR(desChng.layers[i].neurons);

		// Contains the desired changes to the next layer
		Vec nextExpect = Vec_newZero(callData[i].size);

		// For all neurons of the layer
		for (u32 j = 0; j < currLay->size; j++) {
			// The neuron to correct
			const Neuron *neur = &nn->layers[i].neurons[j];

			desChng.layers[i].neurons[j].func = neur->func;

			// Calculate the output of the derivative
			double derivOut = ActivFunc_callDeriv(neur->func,
				Vec_get(currLay, j));
			/* Find in which way the weights and bias whould be
			adjusted */
			double chngStep = 2.0 * (Vec_get(&prevExpect, j) -
				Vec_get(currLay, j));

			// Find the desired change in the weight
			Vec wghtsDes = NeuronDesiredWghtsChng(
				&callData[i],  // The next layer's outputs
				derivOut, chngStep);
			desChng.layers[i].neurons[j].weights = wghtsDes;

			// Find the desired change in the bias
			double biasDes = NeuronDesiredBiasChng(derivOut,
				chngStep);
			desChng.layers[i].neurons[j].bias = biasDes;

			// Find the desired change in the next layer
			Vec neurDes = NeuronDesiredPrevLayChng(&neur->weights,
				derivOut, chngStep);

			Vec_add(&nextExpect, &neurDes);
			Vec_del(&neurDes);
		}

		// Setting the next layer expectations
		if (currLay->size > 0)
			Vec_scl(&nextExpect, 1.0 / (double) currLay->size);

		Vec_del(&prevExpect);
		prevExpect = nextExpect;
	}

	Vec_del(&prevExpect);

	return desChng;
}


Net AverageDesires(const Net *nns, u32 nnsAmnt)
{
	if (nnsAmnt < 1) {
		printf("AverageDesires: nnsAmnt parametter is %d !\n", nnsAmnt);
		exit(EXIT_FAILURE);
	}

	Net avrg;
	avrg.size = nns[0].size;
	avrg.inLen = nns[0].inLen;
	avrg.outLen = nns[0].inLen;

	avrg.layers = NEW_ARR(Layer, avrg.size);
	CHECK_PTR(avrg.layers);

	// For all layers
	for (u32 l = 0; l < avrg.size; l++) {
		Layer lay;
		lay.size = nns[0].layers[l].size;
		lay.neurons = NEW_ARR(Neuron, lay.size);
		CHECK_PTR(lay.neurons);

		// For all neurons
		for (u32 n = 0; n < lay.size; n++) {
			Neuron neur;
			neur.bias = 0.0;
			neur.weights = Vec_newZero(
				nns[0].layers[l].neurons[0].weights.size);
			
			for (u32 i = 0; i < nnsAmnt; i++) {
				neur.bias += nns[i].layers[l].neurons[n].bias;
				Vec_add(&neur.weights,
					&nns[i].layers[l].neurons[n].weights);
			}

			neur.bias /= (double) nnsAmnt;
			Vec_scl(&neur.weights, 1.0 / (double) nnsAmnt);

			lay.neurons[n] = neur;
		}

		avrg.layers[l] = lay;
	}

	return avrg;
}


double EvalLoss(const Mat *real, const Mat *expect)
{
	double res = 0.0;

	for (u32 x = 0; x < real->width; x++) {
		for (u32 y = 0; y < real->height; y++) {
			double val = Mat_get(real, x, y) -
				Mat_get(expect, x, y);
			res += val * val;
		}
	}

	res /= (double) real->height;

	return res;
}


void ApplyChng(Net *nn, const Net *chng)
{
	// For all layers
	for (u32 l = 0; l < nn->size; l++) {
		// For all neurons
		for (u32 n = 0; n < nn->layers[l].size; n++) {
			// Add weights changes
			Vec_add(&nn->layers[l].neurons[n].weights,
				&chng->layers[l].neurons[n].weights);
			// Add bias change
			nn->layers[l].neurons[n].bias +=
				chng->layers[l].neurons[n].bias;
		}
	}
}


void BackProp(Net *nn, const Mat *ins, const Mat *expect)
{
	/*if (ins->width != expect->width || ins->height != expect->height) {
		printf("BackProp: ins and expected have different formats ! (%d;%d) != (%d;%d)\n",
			ins->width, ins->height, expect->width, expect->height);
		exit(EXIT_FAILURE);
	}*/

	/*Mat rOuts = Net_fwdBatch(nn, ins);
	double loss = EvalLoss(&rOuts, expect);
	// if (loss > 1.0)
	// 	loss = 1.0;
	Mat_del(&rOuts);*/

	Net *desList;
	desList = NEW_ARR(Net, ins->height);
	CHECK_PTR(desList);

	for (u32 i = 0; i < ins->height; i++) {
		// Get inputs
		Vec inV = Mat_getRowOf(ins, i);

		Vec *callData;
		u32 callDatLen;

		CalcCallData(nn, &inV, &callData, &callDatLen);
		Vec_del(&inV);

		// Desired changes
		Vec expV = Mat_getRowOf(expect, i);
		Net des = GetDesChng(nn, callData, callDatLen, &expV);  // loss);

		desList[i] = des;
		Vec_del(&expV);

		for (u32 i = 0; i < callData->size; i++)
			Vec_del(callData);
		DEL(callData);
	}

	Net avrgChng = AverageDesires(desList, ins->height);

	// Clear individual desires
	for (u32 i = 0; i < ins->height; i++)
		Net_del(&desList[i]);
	DEL(desList);

	ApplyChng(nn, &avrgChng);

	Net_del(&avrgChng);
}