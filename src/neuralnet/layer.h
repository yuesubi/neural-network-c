#ifndef NN_LAYER_H
#define NN_LAYER_H

#include "activ_funcs.h"
#include "custom_types.h"
#include "neuron.h"
#include "math/vector.h"


typedef struct Layer {
	Neuron* neurons;
	u32 size;
} Layer;


/* Get a layer which as neurons with random weights and biases. Free with
Layer_del when not used anymore. */
Layer Layer_rnd(ActivFunc function, u32 inputAmount, u32 layerSize);
// Pass inputs through the layer
Vec Layer_fwd(const Layer *layer, const Vec *inputs);

/* Pass a batch of inputs through the layer and returns layer's outputs
 *
 * layer: The layer to forward the batch of inputs through.
 * inputBatch: The inputs used. They should be in the format were one line is 
 * all the inputs of the previous layer.
 * 
 * return: the outputs returned are in the form were each row of output is the
 * output of one set of inputs passed through this layer.
 */
Mat Layer_fwdBatch(const Layer *layer, const Mat *inputBatch);

// Free the memory of the layer
void Layer_del(Layer *layer);


#endif  // NN_NEURAL_H