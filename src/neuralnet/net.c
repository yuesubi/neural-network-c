#include "net.h"

#include <stdio.h>

#include "macros.h"


Net Net_rnd(ActivFunc func, const u32 *cfg, u32 cfgLen)
{
	Net n;

#ifndef RELEASE
	if (cfgLen < 2)
		ERR("Net_rnd cann't construct a neural network of less than 2 layers");
#endif

	n.layers = NEW_ARR(Layer, cfgLen - 1);
	for (u32 i = 1; i < cfgLen; i++)
		n.layers[i - 1] = Layer_rnd(func, cfg[i - 1], cfg[i]);

	n.size = cfgLen - 1;

	n.inLen = cfg[0];
	n.outLen = cfg[cfgLen - 1];

	return n;
}


static Vec ReadVec(FILE *fptr)
{
	Vec v; 

	fscanf(fptr, "v{%d,[", &v.size);
	v = Vec_new(v.size);

	for (u32 i = 0; i < v.size; i++) {
		if (i != 0)
			fscanf(fptr, ",");
		double val;
		fscanf(fptr, "%f", &val);
		Vec_set(&v, i, val);
	}

	fscanf(fptr, "]}");

	return v;
}

static Neuron ReadNeuron(FILE *fptr)
{
	Neuron n;

	fscanf(fptr, "n{%d,%f,", &n.func, &n.bias);
	n.weights = ReadVec(fptr);
	fscanf(fptr, "}");

	return n;
}

static Layer ReadLayer(FILE *fptr)
{
	Layer l;

	fscanf(fptr, "l{%d,[", &l.size);
	l.neurons = NEW_ARR(Neuron, l.size);
	CHECK_PTR(l.neurons);

	for (u32 n = 0; n < l.size; n++) {
		if (n != 0)
			fscanf(fptr, ",");
		l.neurons[n] = ReadNeuron(fptr);
	}

	fscanf(fptr, "]}");

	return l;
}

Net Net_load(const char *filePath)
{
	Net nn;

	FILE *fptr = fopen(filePath, "r");
	CHECK_PTR(fptr);

	fscanf(fptr, "w{%d,%d,%d,[", &nn.size, &nn.inLen, &nn.outLen);
	
	nn.layers = NEW_ARR(Layer, nn.size);
	CHECK_PTR(nn.layers);

	for (u32 l = 0; l < nn.size; l++) {
		if (l != 0)
			fscanf(fptr, ",");
		nn.layers[l] = ReadLayer(fptr);
	}

	fscanf(fptr, "]}\n");

	return nn;
}


static void WriteVec(FILE *fptr, const Vec *v)
{
	fprintf(fptr, "v{%d,[", v->size);

	for (u32 i = 0; i < v->size; i++) {
		if (i != 0)
			fprintf(fptr, ",");
		fprintf(fptr, "%f", Vec_get(v, i));
	}

	fprintf(fptr, "]}");
}

static void WriteNeuron(FILE *fptr, const Neuron *n)
{
	fprintf(fptr, "n{%d,%f,", n->func, n->bias);
	WriteVec(fptr, &n->weights);
	fprintf(fptr, "}");
}

static void WriteLayer(FILE *fptr, const Layer *l)
{
	fprintf(fptr, "l{%d,[", l->size);

	for (u32 n = 0; n < l->size; n++) {
		if (n != 0)
			fprintf(fptr, ",");
		WriteNeuron(fptr, &l->neurons[n]);
	}

	fprintf(fptr, "]}");
}

void Net_save(const Net *nn, const char *filePath)
{
	FILE *fptr = fopen(filePath, "w");
	CHECK_PTR(fptr);

	fprintf(fptr, "w{%d,%d,%d,[", nn->size, nn->inLen, nn->outLen);

	for (u32 l = 0; l < nn->size; l++) {
		if (l != 0)
			fprintf(fptr, ",");
		WriteLayer(fptr, &nn->layers[l]);
	}

	fprintf(fptr, "]}\n");

	fclose(fptr);
}

Vec Net_fwd(const Net *n, const Vec *ins)
{
	Vec outs = Layer_fwd(&n->layers[0], ins);

	for (u32 i = 1; i < n->size; i++) {
		Vec newOuts = Layer_fwd(&n->layers[i], &outs);
		Vec_del(&outs);
		outs = newOuts;
	}

	return outs;
}

Mat Net_fwdBatch(const Net *n, const Mat *ins)
{
	Mat outs = Layer_fwdBatch(&n->layers[0], ins);

	for (u32 i = 1; i < n->size; i++) {
		Mat newOuts = Layer_fwdBatch(&n->layers[i], &outs);
		Mat_del(&outs);
		outs = newOuts;
	}

	return outs;
}

void Net_print(const Net *n)
{
	for (u32 i = 0; i < n->size; i++) {
		const Layer *l = &n->layers[i];
		printf("%d | ", i + 1);

		for (u32 j = 0; j < l->size; j++) {
			const Neuron *neur = &l->neurons[j];
			printf("[");

			for (u32 k = 0; k < neur->weights.size; k++) {
				double val = Vec_get(&neur->weights, k);
				printf("%f; ", val);
			}

			printf("bias: %f] ", neur->bias);
		}

		printf("\n");
	}
}

void Net_del(Net *n)
{
	for (u32 i = 0; i < n->size; i++)
		Layer_del(&n->layers[i]);
	DEL(n->layers);
}