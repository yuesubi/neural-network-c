#ifndef NN_CORE_H
#define NN_CORE_H


#include "back_prop/back_prop.h"

#include "math/matrix.h"
#include "math/vector.h"

#include "activ_funcs.h"
#include "custom_types.h"
#include "layer.h"
#include "macros.h"
#include "net.h"
#include "neuron.h"
#include "rnd.h"


#endif  // NN_CORE_H