#ifndef NN_NEURON_H
#define NN_NEURON_H

#include <stdlib.h>

#include "activ_funcs.h"
#include "custom_types.h"
#include "math/matrix.h"
#include "math/vector.h"


typedef struct Neuron {
	ActivFunc func;
	Vec weights;
	double bias;
} Neuron;


/* Generate a neuron with random weights and bias. Free with Neuron_del when not
used anymore. */
Neuron Neuron_rnd(ActivFunc activationFunction, u32 inputsAmount);
// Pass inputs through the neuron
double Neuron_fwd(const Neuron *neuron, const Vec *inputs);

/* Pass a batch of inputs through the neuron and returns the neuron outputs
 *
 * neuron: The neuron used to forward the batch of inputs.
 * inputBatch: The inputs used. They should be in the format were one line is 
 * all the inputs of the previous layer.
 * 
 * return: the outputs returned are in the form were each value of the output
 * vector is the output of one set of inputs.
 */
Vec Neuron_fwdBatch(const Neuron *neuron, const Mat *inputBatch);

// Free the memory of the neuron
void Neuron_del(Neuron *neuron);


#endif  // NN_NEURON_H