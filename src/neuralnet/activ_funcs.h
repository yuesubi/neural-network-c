#ifndef NN_ACTIV_FUNCS_H
#define NN_ACTIV_FUNCS_H


typedef enum ActivFunc {
	ActivFuncNone = 0,
	ActivFuncReLU = 1,
	ActivFuncSigmoid = 2
} ActivFunc;


double ActivFunc_call(ActivFunc function, double x);
double ActivFunc_callDeriv(ActivFunc function, double x);


#endif  // NN_ACTIV_FUNCS_H