#include "layer.h"

#include "macros.h"


Layer Layer_rnd(ActivFunc func, u32 inpAmnt, u32 layerSize)
{
	Layer l;

	l.neurons = NEW_ARR(Neuron, layerSize);
	CHECK_PTR(l.neurons);

	for (u32 i = 0; i < layerSize; i++)
		l.neurons[i] = Neuron_rnd(func, inpAmnt);

	l.size = layerSize;

	return l;
}

Vec Layer_fwd(const Layer *l, const Vec *ins)
{
	Vec outs = Vec_new(l->size);

	for (u32 i = 0; i < l->size; i++) {
		double res = Neuron_fwd(&l->neurons[i], ins);
		Vec_set(&outs, i, res);
	}

	return outs;
}

Mat Layer_fwdBatch(const Layer *l, const Mat *inBat)
{
	Mat out = Mat_new(l->size, inBat->height);

	for (u32 i = 0; i < l->size; i++) {
		Vec res = Neuron_fwdBatch(&l->neurons[i], inBat);
		Mat_setCol(&out, i, &res);
		Vec_del(&res);
	}

	return out;
}

void Layer_del(Layer *l)
{
	for (i32 i = 0; i < l->size; i++) {
		Neuron_del(&l->neurons[i]);
	}

	DEL(l->neurons);
}