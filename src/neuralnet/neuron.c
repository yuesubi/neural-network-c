#include "neuron.h"

#include "macros.h"
#include "rnd.h"


Neuron Neuron_rnd(ActivFunc activFunc, u32 inputs)
{
	Neuron n;

	n.func = activFunc;

	n.weights = Vec_new(inputs);

	for (u32 i = 0; i < n.weights.size; i++)
		Vec_set(&n.weights, i, Rnd_double(-1.0, 1.0, 0.001));

	n.bias = Rnd_double(-1.0, 1.0, 0.001);

	return n;
}

double Neuron_fwd(const Neuron *n, const Vec *inputs)
{
	double res = n->bias;

	for (u32 i = 0; i < n->weights.size; i++)
		res += Vec_get(inputs, i) * Vec_get(&n->weights, i);

	return ActivFunc_call(n->func, res);
}

Vec Neuron_fwdBatch(const Neuron *n, const Mat *inBat)
{
	Vec out = Vec_new(inBat->height);

	for (u32 y = 0; y < out.size; y++) {
		double res = n->bias;

		for (u32 i = 0; i < inBat->width; i++) {
			res += Mat_get(inBat, i, y) * Vec_get(&n->weights, i);
		}

		res = ActivFunc_call(n->func, res);
		Vec_set(&out, y, res);
	}

	return out;
}

void Neuron_del(Neuron *n)
{
	Vec_del(&n->weights);
}