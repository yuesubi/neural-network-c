#include "matrix.h"

#include "../macros.h"


#define XORSWAP(var1,var2) {       \
	if ((var1) != (var2)) {    \
		(var1) ^= (var2);  \
		(var2) ^= (var1);  \
		(var1) ^= (var2);  \
	}                          \
}


static inline void SetVal(Mat *m, u32 x, u32 y, double val)
{
	m->m_Vals[x + m->width * y] = val;
}

static inline double GetVal(const Mat *m, u32 x, u32 y)
{
	return m->m_Vals[x + m->width * y];
}

Mat Mat_new(u32 width, u32 height)
{
	Mat m;

	m.width = width;
	m.height = height;

	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	return m;
}

Mat Mat_newDef(u32 width, u32 height, double defaultVal)
{
	Mat m;

	m.width = width;
	m.height = height;

	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	for (u64 i = 0; i < (u64) m.width * m.height; i++)
		m.m_Vals[i] = defaultVal;

	return m;
}

Mat Mat_newIdty(u32 size)
{
	Mat m;

	m.width = size;
	m.height = size;

	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	for (u32 x = 0; x < m.width; x++) {
		for (u32 y = 0; y < m.width; y++) {
			SetVal(&m, x, y, (x == y)? 1.0 : 0.0);
		}
	}

	return m;
}

Mat Mat_cpy(const Mat *origin)
{
	Mat m = *origin;

	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	for (u64 i = 0; i < (u64) m.width * m.height; i++)
		m.m_Vals[i] = origin->m_Vals[i];
	
	return m;
}

Mat Mat_fromVec(Vec *v)
{
	Mat m;

	m.width = 1;
	m.height = v->size;

	m.m_Vals = Vec_getVals(v);

	return m;
}

Mat Mat_newFromVec(const Vec *v)
{
	Mat m = Mat_new(1, v->size);

	for (u32 y = 0; y < m.height; y++) {
		Mat_set(&m, 0, y, Vec_get(v, y));
	}

	return m;
}

void Mat_set(Mat *m, u32 x, u32 y, double val)
{
	SetVal(m, x, y, val);
}
double Mat_get(const Mat *m, u32 x, u32 y)
{
	return GetVal(m, x, y);
}

void Mat_setRow(Mat *m, u32 y, const Vec *row)
{
	for (u32 x = 0; x < m->width; x++) {
		SetVal(m, x, y, Vec_get(row, x));
	}
}

Vec Mat_getRowOf(const Mat *m, u32 y)
{
	Vec v = Vec_new(m->width);

	for (u32 x = 0; x < v.size; x++) {
		Vec_set(&v, x, GetVal(m, x, y));
	}

	return v;
}

void Mat_setCol(Mat *m, u32 x, const Vec *col)
{
	for (u32 y = 0; y < m->height; y++) {
		SetVal(m, x, y, Vec_get(col, y));
	}
}

Vec Mat_getColOf(const Mat *m, u32 x)
{
	Vec v = Vec_new(m->height);

	for (u32 y = 0; y < v.size; y++) {
		Vec_set(&v, y, GetVal(m, x, y));
	}

	return v;
}

void Mat_add(Mat *mat, const Mat *other)
{
#ifndef RELEASE
	if (mat->width != other->width || mat->height != other->height) {
		printf("Mat_add can't work on matrices of different form. (%dx%d;%dx%d)\n",
			mat->width, mat->height, other->width, other->height);
		exit(EXIT_FAILURE);
	}
#endif

	for (u32 x = 0; x < mat->width; x++) {
		for (u32 y = 0; y < mat->height; y++) {
			SetVal(mat, x, y, GetVal(mat, x, y) +
				GetVal(other, x, y));
		}
	}
}

Mat Mat_addOf(const Mat *mat, const Mat *other)
{
#ifndef RELEASE
	if (mat->width != other->width || mat->height != other->height) {
		printf("Mat_addOf can't work on matrices of different form. (%dx%d;%dx%d)\n",
			mat->width, mat->height, other->width, other->height);
		exit(EXIT_FAILURE);
	}
#endif
	Mat newMat;
	newMat.m_Vals = NEW_ARR(double, mat->width * mat->height);
	CHECK_PTR(newMat.m_Vals);
	newMat.width = mat->width;
	newMat.height = mat->height;

	for (u32 x = 0; x < mat->width; x++) {
		for (u32 y = 0; y < mat->height; y++) {
			SetVal(&newMat, x, y, GetVal(mat, x, y) +
				GetVal(other, x, y));
		}
	}

	return newMat;
}

void Mat_sub(Mat *mat, const Mat *other)
{
#ifndef RELEASE
	if (mat->width != other->width || mat->height != other->height) {
		printf("Mat_sub can't work on matrices of different form. (%dx%d;%dx%d)\n",
			mat->width, mat->height, other->width, other->height);
		exit(EXIT_FAILURE);
	}
#endif

	for (u32 x = 0; x < mat->width; x++) {
		for (u32 y = 0; y < mat->height; y++) {
			SetVal(mat, x, y, GetVal(mat, x, y) -
				GetVal(other, x, y));
		}
	}
}

Mat Mat_subOf(const Mat *mat, const Mat *other)
{
#ifndef RELEASE
	if (mat->width != other->width || mat->height != other->height) {
		printf("Mat_subOf can't work on matrices of different form. (%dx%d;%dx%d)\n",
			mat->width, mat->height, other->width, other->height);
		exit(EXIT_FAILURE);
	}
#endif
	Mat newMat;
	newMat.m_Vals = NEW_ARR(double, mat->width * mat->height);
	CHECK_PTR(newMat.m_Vals);
	newMat.width = mat->width;
	newMat.height = mat->height;

	for (u32 x = 0; x < mat->width; x++) {
		for (u32 y = 0; y < mat->height; y++) {
			SetVal(&newMat, x, y, GetVal(mat, x, y) -
				GetVal(other, x, y));
		}
	}

	return newMat;
}

Mat Mat_mulOf(const Mat *mat, const Mat *other)
{
#ifndef RELEASE
	if (mat->width != other->height) {
		printf("Mat_mulOf can't work on matrices that have width different to the other's height. ([%d]x%d;%dx[%d]) the numbers in [] need to be the same.\n",
			mat->width, mat->height, other->width, other->height);
		exit(EXIT_FAILURE);
	}
#endif
	Mat m;
	m.width = other->width;
	m.height = mat->height;
	m.m_Vals = NEW_ARR(double, m.width * m.height);

	for (u32 x = 0; x < m.width; x++) {
		for (u32 y = 0; y < m.height ; y++) {
			double res = 0.0;

			for (u32 i = 0; i < mat->width; i++)
				res += GetVal(mat, i, y) * GetVal(other, x, i);

			SetVal(&m, x, y, res);
		}
	}

	return m;
}

void Mat_scl(Mat *m, double sclr)
{
	for (u64 i = 0; i < m->width * m->height; i++)
		m->m_Vals[i] *= sclr;
}

Mat Mat_sclOf(const Mat *mat, double sclr)
{
	Mat m = *mat;
	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	for (u64 i = 0; i < m.width * m.height; i++)
		m.m_Vals[i] = mat->m_Vals[i] * sclr;
	
	return m;
}

void Mat_inv(Mat *mat)
{	
	// TODO: try to inverse without doing a malloc

	XORSWAP(mat->width, mat->height);

	double *tmp = NEW_ARR(double, mat->width * mat->height);
	CHECK_PTR(tmp);

	for (u32 x = 0; x < mat->width; x++) {
		for (u32 y = 0; y < mat->height ; y++) {
			tmp[x + y * mat->width] = mat->m_Vals[y + x *
				mat->height];
		}
	}

	DEL(mat->m_Vals);
	mat->m_Vals = tmp;
}

Mat Mat_invOf(const Mat *mat)
{
	Mat m;

	m.width = mat->height;
	m.height = mat->width;

	m.m_Vals = NEW_ARR(double, m.width * m.height);
	CHECK_PTR(m.m_Vals);

	for (u32 x = 0; x < m.width; x++) {
		for (u32 y = 0; y < m.height ; y++) {
			SetVal(&m, x, y, GetVal(mat, y, x));
		}
	}

	return m;
}

void Mat_print(const Mat *m)
{
	printf("<><>< Mat %dx%d ><><>\n", m->width, m->height);
	for (u32 y = 0; y < m->height ; y++) {
		for (u32 x = 0; x < m->width; x++) {
			printf("%.2f; ", GetVal(m, x, y));
		}
		printf("\n");
	}
	printf("<><><      ><><>\n");
}

void Mat_del(Mat *m)
{
	DEL(m->m_Vals);
	m->width = 0;
	m->height = 0;
}