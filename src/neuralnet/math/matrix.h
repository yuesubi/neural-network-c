#ifndef NN_MATH_MATRIX_H
#define NN_MATH_MATRIX_H

#include "../custom_types.h"
#include "vector.h"


// A matrix of M x N form 
typedef struct Mat {
	double *m_Vals;
	u32 width, height;
} Mat;


/* Creates a matrix with enougth memory. No values of the matrix are
initialized. Free the memory with Mat_del when not used anymore. */
Mat Mat_new(u32 width, u32 height);
/* Creates a matrix with enougth memory, with all values being defaultValue.
Free the memory with Mat_del when not used anymore. */
Mat Mat_newDef(u32 width, u32 height, double defaultValue);
/* Creates an identity matrix. Free the memory with Mat_del when not used
anymore. */
Mat Mat_newIdty(u32 size);

/* Creates a matrix witch is an exact copy of the given one. Free the memory
with Mat_del when not used anymore.*/
Mat Mat_cpy(const Mat *matixToCopy);

/* Creates a matrix of width 1 from the given vector */
Mat Mat_fromVec(Vec *vector);
/* Creates a matrix of width 1 from the given vector. Free the memory with
Mat_del when not used anymore.*/
Mat Mat_newFromVec(const Vec *vector);

// Getters and setters

void Mat_set(Mat *matrix, u32 x, u32 y, double value);
double Mat_get(const Mat *matrix, u32 x, u32 y);

/* Sets the row with the content of the vector */
void Mat_setRow(Mat *matrix, u32 y, const Vec *row);
/* Creates a vector with a copy of the content of the row. Free the memory with
Vec_del when not used anymore */
Vec Mat_getRowOf(const Mat *matrix, u32 y);
/* Sets the column with the content of the vector */
void Mat_setCol(Mat *matrix, u32 x, const Vec *column);
/* Creates a vector with a copy of the content of the column. Free the memory
with Vec_del when not used anymore */
Vec Mat_getColOf(const Mat *matrix, u32 x);

// Operations

// Add the matrix matrixToAdd to matrixToModify. matrixToModify will be modified
void Mat_add(Mat *matrixToModify, const Mat *matrixToAdd);
// Get the result of the addition of two matrices
Mat Mat_addOf(const Mat *matrix, const Mat *otherMatrix);

/* Substract the matrix matrixToSubstract from matrixToModify. matrixToModify
will be modified */
void Mat_sub(Mat *matrixToModify, const Mat *matrixToSubstract);
// Get the result of the substraction of two matrices
Mat Mat_subOf(const Mat *matrix, const Mat *otherMatrix);

// Get the result of the multiplication of two matrices
Mat Mat_mulOf(const Mat *matrix, const Mat *otherMatrix);

// Scale the matrixToModify by the scalar
void Mat_scl(Mat *matrixToModify, double scalar);
// Get the result of this matrix scaled by the scalar
Mat Mat_sclOf(const Mat *matrix, double scalar);

// Inverse the matrix. The input matrix will be modified
void Mat_inv(Mat *matrixToModify);
// Get the inverse of the matrix.
Mat Mat_invOf(const Mat *matrixToModify);

// Print the matrix
void Mat_print(const Mat *matrix);

// Frees the memory of the values in the matrix but to the matrix object
void Mat_del(Mat *matrix);


#endif  // NN_MATH_MATRIX_H