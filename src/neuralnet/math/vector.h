#ifndef NN_MATH_VECTOR_H
#define NN_MATH_VECTOR_H

#include "../custom_types.h"


// A vector of N dimentions
typedef struct Vec {
	double *m_Vals;
	u32 size;
} Vec;


/* Create a new vector of n dimentions with unitialized values. The memory will
need to be deleted with Vec_del. */
Vec Vec_new(u32 size);
/* Create a new vector of n dimentions with values defaultValue. The memory will
need to be deleted with Vec_del. */
Vec Vec_newDef(u32 size, double defaultValue);
/* Create a new vector of n dimentions with values of 0. The memory will need to
be deleted with Vec_del. */
Vec Vec_newZero(u32 size);

// Creates a new vector with the same values as the one given
Vec Vec_cpy(const Vec *vector);

// Get the value at the index
double Vec_get(const Vec *vector, u32 index);
// Set the value to the index
void Vec_set(const Vec *vector, u32 index, double value);

// Get values of the vector
double *Vec_getVals(Vec *vector);

// Calculates the length of the vectorr. (more expensive then Vec_sqrdLen)
double Vec_len(const Vec *vector);
// Calculates the squared length of the vector. (less expensive then Vec_len)
double Vec_sqrdLen(const Vec *vector);

// Calculate the dot product of the vector
double Vec_dot(const Vec *vector, const Vec *other);

// Adds the other to the vectorToModify
void Vec_add(Vec *vectorToModify, const Vec *other);
// Return the addition of two vectors
Vec Vec_addOf(const Vec *vector, const Vec *other);

// Substracts the other from the vectorToModify
void Vec_sub(Vec *vectorToModify, const Vec *other);
// Return the substraction of two vectors
Vec Vec_subOf(const Vec *vector, const Vec *other);

// Scales the vectorToModify by the scalar
void Vec_scl(Vec *vectorToModify, double scalar);
// Returns the vector scaled by the scalar
Vec Vec_sclOf(const Vec *vector, double scalar);

// Frees the memory allocated with Vec_new or Vec_zero.
void Vec_del(Vec *vector);


#endif  // NN_MATH_VECTOR_H