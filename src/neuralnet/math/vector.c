#include "vector.h"

#include <math.h>

#include "../macros.h"


Vec Vec_new(u32 size)
{
	Vec v;

	v.size = size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);
	
	return v;
}

Vec Vec_newDef(u32 size, double val)
{
	Vec v;

	v.size = size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = val;
	
	return v;
}

Vec Vec_newZero(u32 size)
{
	Vec v;

	v.size = size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = 0.0;
	
	return v;
}

Vec Vec_cpy(const Vec *origin)
{
	Vec v = *origin;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = origin->m_Vals[i];
	
	return v;
}

double Vec_get(const Vec *v, u32 i)
{
	return v->m_Vals[i];
}

void Vec_set(const Vec *v, u32 i, double val)
{
	v->m_Vals[i] = val;
}

double *Vec_getVals(Vec *v)
{
	return v->m_Vals;
}

double Vec_len(const Vec *v)
{
	double sum = 0.0;

	for (u32 i = 0; i < v->size; i++)
		sum += v->m_Vals[i] * v->m_Vals[i];
	
	return sqrt(sum);
}

double Vec_sqrdLen(const Vec *v)
{
	double sum = 0.0;

	for (u32 i = 0; i < v->size; i++)
		sum += v->m_Vals[i] * v->m_Vals[i];

	return sum;
}

double Vec_dot(const Vec *v, const Vec *oth)
{
#ifndef RELEASE
	if (v->size != oth->size) {
		printf("Vec_dot can't work on vectors of different sizes. %d != %d\n",
			v->size, oth->size);
		exit(EXIT_FAILURE);
	}
#endif

	double dot = 0.0;

	for (u32 i = 0; i < v->size; i++)
		dot += v->m_Vals[i] * oth->m_Vals[i];

	return dot;
}

void Vec_add(Vec *v, const Vec *oth)
{
#ifndef RELEASE
	if (v->size != oth->size) {
		printf("Vec_add can't work on vectors of different sizes. %d != %d\n",
			v->size, oth->size);
		exit(EXIT_FAILURE);
	}
#endif

	for (u32 i = 0; i < v->size; i++)
		v->m_Vals[i] += oth->m_Vals[i];
}

Vec Vec_addOf(const Vec *vec, const Vec *oth)
{
#ifndef RELEASE
	if (vec->size != oth->size) {
		printf("Vec_addOf can't work on vectors of different sizes. %d != %d\n",
			vec->size, oth->size);
		exit(EXIT_FAILURE);
	}
#endif

	Vec v;

	v.size = vec->size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = vec->m_Vals[i] + oth->m_Vals[i];

	return v;
}

void Vec_sub(Vec *v, const Vec *oth)
{
#ifndef RELEASE
	if (v->size != oth->size) {
		printf("Vec_sub can't work on vectors of different sizes. %d != %d\n",
			v->size, oth->size);
		exit(EXIT_FAILURE);
	}
#endif

	for (u32 i = 0; i < v->size; i++)
		v->m_Vals[i] -= oth->m_Vals[i];
}

Vec Vec_subOf(const Vec *vec, const Vec *oth)
{
#ifndef RELEASE
	if (vec->size != oth->size) {
		printf("Vec_subOf can't work on vectors of different sizes. %d != %d\n",
			vec->size, oth->size);
		exit(EXIT_FAILURE);
	}
#endif

	Vec v;

	v.size = vec->size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = vec->m_Vals[i] - oth->m_Vals[i];

	return v;
}

void Vec_scl(Vec *v, double sclr)
{
	for (u32 i = 0; i < v->size; i++)
		v->m_Vals[i] -= sclr;
}

Vec Vec_sclOf(const Vec *vec, double sclr)
{
	Vec v;

	v.size = vec->size;

	v.m_Vals = NEW_ARR(double, v.size);
	CHECK_PTR(v.m_Vals);

	for (u32 i = 0; i < v.size; i++)
		v.m_Vals[i] = vec->m_Vals[i] * sclr;

	return v;
}

void Vec_del(Vec *v)
{
	DEL(v->m_Vals);
	v->size = 0;
}