#ifndef NN_NET_H
#define NN_NET_H

#include "custom_types.h"
#include "layer.h"


// A neural network
typedef struct Net {
	Layer *layers;
	u32 size;

	u32 inLen;
	u32 outLen;
} Net;


/* Create a neural network with neurons which have random weights ans biases. 
Free with Net_del when not used anymore. */
Net Net_rnd(ActivFunc function, const u32 *layout, u32 layoutLen);

// Load a neural network from a file. Free with Net_del when not used anymore.
Net Net_load(const char *filePath);
// Save a neural network to a file.
void Net_save(const Net *neuralNetwork, const char *filePath);

// Pass inputs through the neural network
Vec Net_fwd(const Net *neuralNetwork, const Vec *inputs);

/* Pass a batch of inputs through the layer and returns layer's outputs
 *
 * neuralNetwork: The neural network to forward the batch of inputs through.
 * inputBatch: The inputs used. They should be in the format were one line is
 * one set of inputs.
 * 
 * return: the outputs returned are in the form were each row of output is the
 * output of one set of inputs passed through the neural network.
 */
Mat Net_fwdBatch(const Net *neuralNetwork, const Mat *inputBatch);

// Prints a representation of the neural network
void Net_print(const Net *neuralNetwork);

// Free the neural network
void Net_del(Net *neuralNetwork);


#endif  // NN_NET_H