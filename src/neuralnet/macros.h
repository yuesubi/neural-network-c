#ifndef NN_MACROS_H
#define NN_MACROS_H

#include <stdio.h>
#include <stdlib.h>


// Create pointers easily
#define NEW(type) ((type *) malloc(sizeof(type)))
#define NEW_ARR(type, amount) ((type *) malloc(sizeof(type) *  \
	(size_t) (amount)))

// Destroying pointers that aren't NULL
#define DEL(ptr) {                     \
	if ((ptr) != NULL) {           \
		free((void *) (ptr));  \
	}                              \
}

// Stop if a pointer id NULL
#define CHECK_PTR(ptr) {                                                     \
	if ((ptr) == NULL) {                                                 \
		printf("CHECK_PTR found a NULL pointer on line %d of %s !",  \
			__LINE__, __FILE__);                                 \
		exit(EXIT_FAILURE);                                          \
	}                                                                    \
}


// PRINT DES INFORMATIONS
#ifndef LOG_LEVEL
	#ifdef RELEASE
		#define LOG_LEVEL 0
	#else
		#define LOG_LEVEL 3
	#endif
#endif

// Print an information
#if LOG_LEVEL >= 3
	#define LOG(x) { printf("(i) %s:%d %s\n", __FILE__, __LINE__, (x)); }
#else
	#define LOG(x) { }
#endif
// Print a warning
#if LOG_LEVEL >= 2
	#define WARN(x) { printf("(!) %s:%d %s\n", __FILE__, __LINE__, (x)); }
#else
	#define WARN(x) { }
#endif
// Print an error
#if LOG_LEVEL >= 1
	#define ERR(x) { printf("[ERROR] %s:%d %s\n", __FILE__, __LINE__, (x));\
		exit(EXIT_FAILURE); }
#else
	#define ERR(x) { }
#endif


#endif  // NN_MACROS_H