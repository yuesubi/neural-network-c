#include "rnd.h"

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>


static bool isSeedSet = false;


static inline void SetSeed()
{
	if (!isSeedSet) {
		srand(time(0));
		isSeedSet = true;
	}
}


double Rnd_double(double lower, double upper, double precision)
{
	SetSeed();
	return (double) (rand() % ((int) (upper / precision) -
		(int) (lower / precision) + 1)) * precision + lower;
}

float Rnd_float(float lower, float upper, float precision)
{
	SetSeed();
	return (float) (rand() % ((int) (upper / precision) -
		(int) (lower / precision) + 1)) * precision + lower;
}

int Rnd_int(int lower, int upper)
{
	SetSeed();
	return (rand() % (upper - lower + 1)) + lower;
}