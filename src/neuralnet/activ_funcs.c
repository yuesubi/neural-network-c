#include "activ_funcs.h"

#include <math.h>

#include "macros.h"


static double ReLU(double x)
{
	return (x > 0.0)? x : 0.0;
}

static double ReLUDeriv(double x)
{
	return (x > 0.0)? 1.0 : 0.0;
}

static double Sigmoid(double x)
{
	return 1 / (1 + exp(-x));
}

static double SigmoidDeriv(double x)
{
	double sig = Sigmoid(x);
	return sig * (1 - sig);
}


double ActivFunc_call(ActivFunc func, double x)
{
	switch (func) {

	case ActivFuncReLU:
		return ReLU(x);
	case ActivFuncSigmoid:
		return Sigmoid(x);

	default:
		break;
	}

#ifndef RELEASE
	WARN("ActivFunc_call: the function passed as an argument doesn't exist");
#endif
	return -1.0;
}

double ActivFunc_callDeriv(ActivFunc func, double x)
{
	switch (func) {

	case ActivFuncReLU:
		return ReLUDeriv(x);
	case ActivFuncSigmoid:
		return SigmoidDeriv(x);

	default:
		break;
	}

#ifndef RELEASE
	WARN("ActivFunc_callDeriv: the function passed as an argument doesn't exist");
#endif
	return -1.0;
}