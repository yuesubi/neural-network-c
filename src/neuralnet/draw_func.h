#ifndef NN_DRAW_FUNC_H
#define NN_DRAW_FUNC_H


void DrawFunc(double (*func)(double), short width, short height, double xStart,
	double xEnd, double yStart, double yEnd);


#endif  // NN_DRAW_FUNC_H