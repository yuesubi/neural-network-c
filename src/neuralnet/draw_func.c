#include "draw_func.h"

#include "custom_types.h"
#include "macros.h"


char GetRizeOverRunChar(double rizeOverRun)
{
	if (rizeOverRun > 3.0 || rizeOverRun < -3.0)
		return '|';
	else if (rizeOverRun > 2.0)
		return '/';
	else if (rizeOverRun < -2.0)
		return '\\';
	else
		return '-';
}


void DrawFunc(double (*func)(double), short width, short height, double xStart,
	double xEnd, double yStart, double yEnd)
{
	int size = (width + 1) * height;
	char *grid = NEW_ARR(char, size + 1);

	for (int i = 0; i < size; i++) {
		if (i % (width + 1) == width) {
			grid[i] = '\n';  // So that printf prints well
		} else {
			grid[i] = ' '; 
		}
	}
	grid[size] = '\0';

	double xRng = xEnd - xStart;
	double yRng = yEnd - yStart;

	for (u16 i = 0; i < width; i++) {
		double ptX = i * xRng / width + xStart;
		double ptY = (*func)(ptX);
		//printf("%f -> %f\n", ptX, ptY);

		if (yStart < ptY && ptY < yEnd) {
			short plotX = i;
			short plotY = (height - 1) - (ptY - yStart) * height /
				yRng;

			double unit = xRng / width;
			char c = GetRizeOverRunChar(((*func)(ptX + unit) - ptY)
				/ unit);
			grid[plotX + plotY * (width + 1)] = c;
		}
	}

	printf("<><>< Function at %p ><><>\n", func);
	printf("%s", grid);
	printf("<><>< -- ><><>\n");

	DEL(grid);
}