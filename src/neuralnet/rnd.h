#ifndef NN_RND_H
#define NN_RND_H


double Rnd_double(double lower, double upper, double precision);
float Rnd_float(float lower, float upper, float precision);
int Rnd_int(int lower, int upper);


#endif  // NN_RND_H