#include <stdio.h>

#include "neuralnet/core.h"


#define CFG_LEN 4


int not_main(void)
{
	u32 layout[CFG_LEN] = { 1, 3, 2, 1 };
	Net nn = Net_rnd(ActivFuncSigmoid, layout, CFG_LEN);

	Mat ins = Mat_new(1, 100);
	for (u32 i = 0; i < ins.width * ins.height; i++) {
		ins.m_Vals[i] = i;
	}

	Mat expect = Mat_new(1, 100);
	for (u32 i = 0; i < expect.width * expect.height; i++) {
		expect.m_Vals[i] = 0;
	}

	Mat rOuts = Net_fwdBatch(&nn, &ins);
	printf("Loss before: %f\n", EvalLoss(&rOuts, &expect));
	Mat_del(&rOuts);

	for (u32 i = 0; i < 10000; i++)
		BackProp(&nn, &ins, &expect);

	rOuts = Net_fwdBatch(&nn, &ins);
	printf("Loss after: %f\n", EvalLoss(&rOuts, &expect));
	Mat_del(&rOuts);

	return 0;
}